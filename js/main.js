Bud = function() {
		var docHeight = $(window).height();
		var footerHeight = $('.footer').height();
		var footerTop = $('.footer').position().top + footerHeight;
		var offset_t = $(window).scrollTop();
		var menu = new Bud.Menu();
		
	
		var wrapHeight = $('.mainPage').height();
   
  /**
		 *	@desc Footer
		 */ 
  fixFooter = function () {
			if (footerTop < docHeight) {
				$('.footer').css('margin-top', (docHeight - footerTop) - 20 + 'px');
			}
		},
		
		/**
		 *	@desc Navigation
		 */
		menuHeight = function () {
			if (wrapHeight < docHeight) {
				$('.mainPage').css('min-height', (docHeight - footerHeight) + 10 + 'px');
			}
		},
		
		/**
		 *	@desc Arrow
		 */		 
		arrowSlide = function () {
		$('.arrow-slide-top').hide();
				$(window).scroll(function(){
					if ($(this).scrollTop() > docHeight) {
						$('.arrow-slide-top').show();
					} else {
						$('.arrow-slide-top').hide();
					}
				}),
				
				/**
		 		*	@desc Arrow - scroll to top of page
		 		*/	
				 $('.arrow-slide-top').click(function() {
			     $("html, body").animate({ scrollTop: 0 }, "slow");
			     return false;
			  });
			},
			
			/**
		 		*	@desc Swipe to close the menu finger
		 		*/
		swipe = function () {
			$(".swipe").swipe({
		  swipeRight:function(event, direction, distance, duration, fingerCount) {
		  		menu.closeAllItem();
		  	 //$(this).hide();  
		  }
			});
			$('.mainPage').swipe('disable');
		},
		
			init = function() {
				fixFooter();
				menuHeight();
				arrowSlide();
				swipe();
			};
			
			init();

		};

Bud.Menu = function() {
	// Private properties/methods:
	var 
		thisObj = this,
		menusArr = new Array(
			document.getElementById('menu'),
			document.getElementById('menuFacebook'),
			document.getElementById('menuYouTube')
		),
		
				
		addItem = function(item) {
			menusArr.push(item);
		},
	
		init = function() {
			initEvents();
		},
		initEvents = function() {
			$('#menuFacebook > a').click(toggleFacebook);
			$('#menuYouTube > a').click(toggleYouTube);
			$('#menuOpen').click(toggleMenu);
			$('#menuClose').click(hideMenu);
		},
		
		closeOtherItem = function(me) {
			Array.prototype.forEach.call(menusArr, function(el, i){
				if (el !== me) {
					$(el).removeClass('selected');
					($(el).attr('id') === 'menu') ? hideMenu() : null;
				}
			});
		},
					
		toggleFacebook = function() {
			closeOtherItem(document.getElementById('menuFacebook'));
			$('#menuFacebook').toggleClass('selected');
			return false;
		},

		toggleYouTube = function() {
			closeOtherItem(document.getElementById('menuYouTube'));
			$('#menuYouTube').toggleClass('selected');
			return false;
		},

		toggleMenu = function() {
			closeOtherItem(document.getElementById('menu'));
			($('.mainPage').hasClass("nav-open")) ? hideMenu() : showMenu();
			return false;
		},
					
		hideMenu = function() {
			$('.mainPage').removeClass("nav-open");
			$('#menu').hide();
			$('.mainPage').swipe('disable');
			hideMenuWrapper();
		},
		
		showMenu = function() {
			$('.mainPage').addClass('nav-open');
			$('#menu').show();
			$('.mainPage').swipe('enable');
			showMenuWrapper();
		},
		
		showMenuWrapper = function() {
			$('#menu-wrap').css('width', '100%');
			$('#menu-wrap').css('height', '100%');
		},

		hideMenuWrapper = function() {
			$('#menu-wrap').css('width', '0');
			$('#menu-wrap').css('height', '0');
		};

	// Public method
	
	this.getMenu = function() {
		return this;
	};
	this.closeAllItem = function() {
		Array.prototype.forEach.call(menusArr, function(el, i){
			$(el).removeClass('selected');
			($(el).attr('id') === 'menu') ? hideMenu() : null;
		});
	},
	
	this.hasOpenItem = function() {
		var ret = false;
		Array.prototype.forEach.call(menusArr, function(el, i){
			if (
				$(el).hasClass('selected') ||
				(
					$(el).attr('id') === 'menu' && 
					$('.mainPage').hasClass('nav-open')
				)
			) { ret = true; }
		});
		return ret;
	};
	
	init();
};






$().ready(function() {
	var bud = new Bud();
	

/*
	$('.mainPage').click(function() {
		if (bud.getMenu().hasOpenItem()) {
			bud.getMenu().closeAllItem();
			// return false;
		}
	});
*/

});
        
